input {
  file {
    path => "/data/*.csv"
    start_position => "beginning"
   sincedb_path => "/dev/null"
  }
}
filter {
  csv {
      separator => ","
     columns => ["type", "Message Wcag", "Criteria", "Context", "Selector"]
  }
}
output {
   elasticsearch {
     hosts => "http://13.211.127.69:9200/"
     index => "Accessibility"
  }
stdout {}
}